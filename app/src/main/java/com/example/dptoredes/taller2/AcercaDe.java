package com.example.dptoredes.taller2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AcercaDe extends AppCompatActivity {

    String tag = "Lifecycle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerca_de);

        TextView user= (TextView)(findViewById(R.id.txt_user));
        user.setText((String)getIntent().getExtras().get("usuario"));

        TextView pass= (TextView)(findViewById(R.id.txt_pass));
        pass.setText((String)getIntent().getExtras().get("password"));

        final Button button = (Button) findViewById(R.id.button6);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Score();

            }
        });
    }

    public void Score() {
        Log.d (tag,"Si llamo a la funcion");

        TextView u = (TextView) (findViewById(R.id.user));
        TextView p = (TextView) (findViewById(R.id.pass));

        Intent i02 = new Intent(this, Score.class);
        String data [] = new String [2];
        i02.putExtra("usuario", ""+u.getText());
        i02.putExtra("password",""+p.getText());
        startActivity(i02);



    }


}
